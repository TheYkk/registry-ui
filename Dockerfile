FROM mhart/alpine-node:8


RUN apk add --no-cache curl

ENV NODE_ENV=production

WORKDIR /app
COPY . .

CMD node --max_old_space_size=250 --abort_on_uncaught_exception src/backend/index.js

HEALTHCHECK --interval=15s --timeout=3s CMD curl -f http://localhost/ || exit 1

